function varargout = deteksi_sobel(varargin)
% DETEKSI_SOBEL MATLAB code for deteksi_sobel.fig
%      DETEKSI_SOBEL, by itself, creates a new DETEKSI_SOBEL or raises the existing
%      singleton*.
%
%      H = DETEKSI_SOBEL returns the handle to a new DETEKSI_SOBEL or the handle to
%      the existing singleton*.
%
%      DETEKSI_SOBEL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DETEKSI_SOBEL.M with the given input arguments.
%
%      DETEKSI_SOBEL('Property','Value',...) creates a new DETEKSI_SOBEL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before deteksi_sobel_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to deteksi_sobel_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help deteksi_sobel

% Last Modified by GUIDE v2.5 18-Oct-2023 09:49:32

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @deteksi_sobel_OpeningFcn, ...
                   'gui_OutputFcn',  @deteksi_sobel_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before deteksi_sobel is made visible.
function deteksi_sobel_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to deteksi_sobel (see VARARGIN)

% Choose default command line output for deteksi_sobel
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes deteksi_sobel wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = deteksi_sobel_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in imageButton.
function imageButton_Callback(hObject, eventdata, handles)
[filename,pathname] = uigetfile({'*.*'});
if ~isequal(filename,0)
 Info = imfinfo(fullfile(pathname,filename));
 if Info.BitDepth == 1
 msgbox('Citra masukan harus citra RGB atau Grayscale');
 return
 elseif Info.BitDepth == 8
 Img_Gray = imread(fullfile(pathname,filename));
 axes(handles.axes1)
 cla('reset')
 imshow(Img_Gray)
 title('Grayscale Image')
 Img_bw = im2bw(Img_Gray,graythresh(Img_Gray));
 axes(handles.axes2)
 cla('reset')
 imshow(Img_bw)
 title('Binary Image')
 else
 Img_Gray = rgb2gray(imread(fullfile(pathname,filename)));
 axes(handles.axes1)
 cla('reset')
 imshow(Img_Gray)
 title('Grayscale Image')
 Img_bw = im2bw(Img_Gray,graythresh(Img_Gray));
 axes(handles.axes2)
 cla('reset')
 imshow(Img_bw)
 title('Binary Image')
 end
else
 return
end 
guidata(hObject,handles); 

% --- Executes on button press in sobelDetection.
function sobelDetection_Callback(hObject, eventdata, handles)
citra_dtSobel  = edge(citra_ori, 'sobel');
